import axios from "../axiosConfig";

export async function getAllBoard() {
  const allBoardsUrl = `/members/me/boards`;
  let boards = await axios.get(allBoardsUrl);
  return boards;
}

export async function getBoardLists(boardId) {
  const listsUrl = `/boards/${boardId}/lists`;
  let lists = await axios.get(listsUrl);
  return lists;
}

export async function getChecklistsForCard(cardId) {
  try {
    const response = await axios.get(`/cards/${cardId}/checklists`);
    return response.data;
  } catch (error) {
    console.error("Error fetching checklists:", error);
    console.error("Axios error details:", error.response);
    throw error;
  }
}

export async function createChecklistItem(checklistId, newItemName) {
  try {
    const response = await axios.post(
      `/checklists/${checklistId}/checkItems`,
      {
        name: newItemName.trim(),
        idChecklist: checklistId,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    return response.data;
  } catch (error) {
    console.error("Error creating checklist item:", error);
    console.error("Axios error details:", error.response);
    throw error;
  }
}

export async function toggleChecklistItemState(
  cardId,
  checklistId,
  itemId,
  currentState
) {
  try {
    const response = await axios.put(
      `/cards/${cardId}/checklist/${checklistId}/checkItem/${itemId}`,
      {
        state: currentState === "complete" ? "incomplete" : "complete",
      }
    );
    return response.data;
  } catch (error) {
    console.error("Error toggling checklist item:", error);
    console.error("Axios error details:", error.response);
    throw error;
  }
}
export async function getChecklistItemsForChecklist(checklistId) {
  try {
    const response = await axios.get(`/checklists/${checklistId}/checkItems`);
    return response.data;
  } catch (error) {
    console.error("Error fetching checklist items:", error);
    return [];
  }
}

export async function deleteChecklistItem(checklistId, itemId) {
  try {
    await axios.delete(`/checklists/${checklistId}/checkItems/${itemId}`);
  } catch (error) {
    console.error("Error deleting checklist item:", error);
    console.error("Axios error details:", error.response);
    throw error;
  }
}

export async function createChecklist(cardId, newChecklistName) {
  try {
    const response = await axios.post(
      "/checklists",
      {
        name: newChecklistName.trim(),
        idCard: cardId,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
    return response.data;
  } catch (error) {
    console.error("Error creating checklist:", error);
    console.error("Axios error details:", error.response);
    throw error;
  }
}

export async function createList(newListName, boardId) {
  try {
    const response = await axios.post(
      "/lists",
      {
        name: newListName.trim(),
        idBoard: boardId,
        cards: [],
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    return response.data;
  } catch (error) {
    console.error("Error creating list:", error);
    console.error("Axios error details:", error.response);
    throw error;
  }
}

export async function createCard(newCardName, listId) {
  try {
    const response = await axios.post(
      "/cards",
      {
        name: newCardName.trim(),
        idList: listId,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    return response.data;
  } catch (error) {
    console.error("Error creating card:", error);
    console.error("Axios error details:", error.response);
    throw error;
  }
}

export async function deleteCard(cardId) {
  try {
    await axios.delete(`/cards/${cardId}`);
  } catch (error) {
    console.error("Error deleting card:", error);
    console.error("Axios error details:", error.response);
    throw error;
  }
}

export async function deleteList(listId) {
  try {
    await axios.put(
      `/lists/${listId}/closed`,
      { value: true },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );
  } catch (error) {
    console.error("Error deleting list:", error);
    console.error("Axios error details:", error.response);
    throw error;
  }
}

export async function getCardsForList(listId) {
  try {
    const response = await axios.get(`/lists/${listId}/cards`);
    return response.data;
  } catch (error) {
    console.error("Error fetching cards:", error);
    console.error("Axios error details:", error.response);
    throw error;
  }
}
export const createBoard = async (boardName) => {
  try {
    const response = await axios.post("/boards", {
      name: boardName,
    });
    return response.data;
  } catch (error) {
    console.error("Error creating board:", error);
    throw error;
  }
};
