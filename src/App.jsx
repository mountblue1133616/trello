import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ResponsiveAppBar from "./components/ResponsiveAppBar";
import AllBoards from "./components/AllBoards";
import BoardCard from "./components/BoardCard";

function App() {
  return (
    <div className="main-container">
      <ResponsiveAppBar />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<AllBoards />} />
          <Route path="/boards/:id" element={<BoardCard />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
