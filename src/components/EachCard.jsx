/* eslint-disable react/prop-types */
import { useState } from "react";
import {
  Card,
  ListItem,
  ListItemText,
  Menu,
  MenuItem,
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
} from "@mui/material";
import {
  MoreVert as MoreVertIcon,
  Delete as DeleteIcon,
} from "@mui/icons-material";

import Checklist from "./Checklist";

const EachCard = ({ cards, handleDeleteCard, listName }) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedCard, setSelectedCard] = useState(null);

  const handleMenuOpen = (event, card) => {
    event.stopPropagation();
    setAnchorEl(event.currentTarget);
    setSelectedCard(card);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    setSelectedCard(null);
  };

  const handleDeleteClick = () => {
    if (selectedCard) {
      handleDeleteCard(selectedCard.id);
    }
    handleMenuClose();
  };

  const handleListItemClick = (card) => {
    setSelectedCard(card);
    setIsModalOpen(true);
  };

  const handleModalClose = () => {
    setSelectedCard(null);
    setIsModalOpen(false);
  };

  return (
    <div>
      {cards.map((card) => (
        <Card key={card.id}>
          <ListItem>
            <ListItemText
              primary={card.name}
              onClick={(e) => {
                e.stopPropagation();
                if (!isModalOpen) {
                  handleListItemClick(card);
                }
              }}
            />
            <Button
              aria-controls={`card-menu-${card.id}`}
              aria-haspopup="true"
              onClick={(event) => handleMenuOpen(event, card)}
              size="small"
            >
              <MoreVertIcon />
            </Button>
            <Menu
              id={`card-menu-${card.id}`}
              anchorEl={anchorEl}
              open={Boolean(
                anchorEl && selectedCard && selectedCard.id === card.id
              )}
              onClose={handleMenuClose}
            >
              <MenuItem onClick={handleDeleteClick}>
                <DeleteIcon />
                Delete Card
              </MenuItem>
            </Menu>
          </ListItem>
        </Card>
      ))}

      <Dialog open={isModalOpen} onClose={handleModalClose}>
        <DialogTitle>
          {listName} - {selectedCard?.name}
        </DialogTitle>
        <DialogContent>
          <Checklist selectedCard={selectedCard} />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleModalClose}>Cancel</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default EachCard;
