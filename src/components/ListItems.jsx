import { useState, useEffect } from "react";
import {
  Card,
  List,
  ListItem,
  ListItemText,
  TextField,
  Button,
  Menu,
  MenuItem,
} from "@mui/material";
import {
  MoreVert as MoreVertIcon,
  Delete as DeleteIcon,
} from "@mui/icons-material";
import {
  createCard,
  deleteCard,
  deleteList,
  getCardsForList,
} from "../TrelloAPI";
import EachCard from "./EachCard";

const ListItems = ({ listItem, handleAddCard }) => {
  const [newCardName, setNewCardName] = useState("");
  const [cards, setCards] = useState([]);
  const [anchorEl, setAnchorEl] = useState(null);
  const [isDeleted, setIsDeleted] = useState(false);

  useEffect(() => {
    const fetchCards = async () => {
      try {
        const fetchedCards = await getCardsForList(listItem.id);
        setCards(fetchedCards);
      } catch (error) {
        console.error("Error fetching cards:", error);
      }
    };

    fetchCards();
  }, [listItem.id]);
  const handleNewCardNameChange = (event) => {
    setNewCardName(event.target.value);
  };

  const handleAddCardClick = async () => {
    if (newCardName.trim() !== "") {
      try {
        const newCard = await createCard(newCardName, listItem.id);
        setNewCardName("");
        setCards((prevCards) => [...prevCards, newCard]);
        handleAddCard(listItem.id, newCard);
      } catch (error) {
        console.error("Error creating card:", error);
      }
    }
  };

  const handleDeleteCard = async (cardId) => {
    try {
      await deleteCard(cardId);
      setCards((prevCards) => prevCards.filter((card) => card.id !== cardId));
    } catch (error) {
      console.error("Error deleting card:", error);
    }
  };

  const handleMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const handleDeleteList = async () => {
    try {
      await deleteList(listItem.id);
      setIsDeleted(true);
    } catch (error) {
      console.error("Error deleting list:", error);
    }
    handleMenuClose();
  };

  if (isDeleted) {
    return null; // Render nothing if the list is deleted
  }

  return (
    <div className="listItems">
      <Card>
        <List>
          <ListItem>
            <ListItemText primary={listItem.name} />
            <Button
              aria-controls="list-menu"
              aria-haspopup="true"
              onClick={handleMenuOpen}
              size="small"
            >
              <MoreVertIcon />
            </Button>
            <Menu
              id="list-menu"
              anchorEl={anchorEl}
              open={Boolean(anchorEl)}
              onClose={handleMenuClose}
            >
              <MenuItem onClick={handleDeleteList}>
                <DeleteIcon />
                Delete List
              </MenuItem>
            </Menu>
          </ListItem>
          <EachCard
            cards={cards}
            handleDeleteCard={handleDeleteCard}
            listName={listItem.name}
          />
        </List>
        <TextField
          label="Card Name"
          value={newCardName}
          onChange={handleNewCardNameChange}
        />
        <Button
          variant="contained"
          color="primary"
          onClick={handleAddCardClick}
        >
          Add Card
        </Button>
      </Card>
    </div>
  );
};

export default ListItems;
