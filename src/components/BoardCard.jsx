import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { getBoardLists } from "../TrelloAPI";
import EachList from "./EachList";

const BoardCard = () => {
  const [list, setList] = useState(null);
  const { id } = useParams();

  useEffect(() => {
    fetchList();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchList = async () => {
    try {
      const response = await getBoardLists(id);
      setList(response.data);
    } catch (error) {
      console.error("Error fetching lists:", error);
    }
  };

  if (!list) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <div className="boardname-bar">
        <h1>Boardname</h1>
        <Link to="/">
          <button className="back-btn">Go back</button>
        </Link>
      </div>
      <EachList list={list} />
    </div>
  );
};

export default BoardCard;
