import { useState } from "react";
import { useParams } from "react-router-dom";
import { Button, TextField } from "@mui/material";
import { createList } from "../TrelloAPI";
import ListItems from "./ListItems";

const EachList = ({ list }) => {
  const [newListName, setNewListName] = useState("");
  const [lists, setLists] = useState(list);
  const { id } = useParams();

  const handleNewListNameChange = (event) => {
    setNewListName(event.target.value);
  };

  const handleAddList = async () => {
    if (newListName.trim() !== "") {
      try {
        const newList = await createList(newListName, id);
        const updatedLists = [...lists, newList];
        setLists(updatedLists);
        setNewListName("");
      } catch (error) {
        console.error("Error creating list:", error);
      }
    }
  };

  const handleAddCard = async (listId, newCard) => {
    const updatedLists = lists.map((listItem) => {
      if (listItem.id === listId) {
        return {
          ...listItem,
          cards: [...(listItem.cards || []), newCard],
        };
      }
      return listItem;
    });

    setLists(updatedLists);
  };

  return (
    <div className="eachList">
      {lists.map((listItem) => (
        <div key={listItem.id}>
          <ListItems listItem={listItem} handleAddCard={handleAddCard} />
        </div>
      ))}
      <div className="addListContainer">
        <TextField
          label="New List Name"
          value={newListName}
          onChange={handleNewListNameChange}
        />
        <Button variant="contained" color="primary" onClick={handleAddList}>
          Add List
        </Button>
      </div>
    </div>
  );
};

export default EachList;
