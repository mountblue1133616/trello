import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { ListItemText, TextField, Button } from "@mui/material";
import ChecklistItem from "./ChecklistItem";
import {
  getChecklistsForCard,
  createChecklistItem,
  toggleChecklistItemState,
  deleteChecklistItem,
  createChecklist,
  getChecklistItemsForChecklist,
} from "../TrelloAPI";

const Checklist = ({ selectedCard }) => {
  const [checklists, setChecklists] = useState([]);
  const [newChecklistName, setNewChecklistName] = useState("");
  const [newItemName, setNewItemName] = useState("");

  useEffect(() => {
    const fetchChecklists = async () => {
      if (!selectedCard) return;

      try {
        const fetchedChecklists = await getChecklistsForCard(selectedCard.id);

        const checklistPromises = fetchedChecklists.map(async (checklist) => {
          const items = await getChecklistItemsForChecklist(checklist.id);
          checklist.items = items;
          return checklist;
        });

        const checklistsWithItems = await Promise.all(checklistPromises);
        setChecklists(checklistsWithItems);
      } catch (error) {
        console.error("Error fetching checklists:", error);
      }
    };

    fetchChecklists();
  }, [selectedCard]);

  const handleToggleChecklistItem = async (checklistId, item) => {
    try {
      const updatedItem = await toggleChecklistItemState(
        selectedCard.id,
        checklistId,
        item.id,
        item.state
      );

      setChecklists((prevChecklists) =>
        prevChecklists.map((cl) =>
          cl.id === checklistId
            ? {
                ...cl,
                items: cl.items.map((i) =>
                  i.id === item.id ? updatedItem : i
                ),
              }
            : cl
        )
      );
    } catch (error) {
      console.error("Error toggling checklist item:", error);
    }
  };

  const handleDeleteChecklistItem = async (checklistId, itemId) => {
    try {
      await deleteChecklistItem(checklistId, itemId);

      setChecklists((prevChecklists) =>
        prevChecklists.map((cl) =>
          cl.id === checklistId
            ? {
                ...cl,
                items: cl.items.filter((i) => i.id !== itemId),
              }
            : cl
        )
      );
    } catch (error) {
      console.error("Error deleting checklist item:", error);
    }
  };

  const handleAddChecklistItem = async (checklist) => {
    if (!checklist || newItemName.trim() === "") return;

    try {
      const newItem = await createChecklistItem(
        checklist.id,
        newItemName.trim()
      );

      setChecklists((prevChecklists) =>
        prevChecklists.map((cl) =>
          cl.id === checklist.id
            ? {
                ...cl,
                items: cl.items ? [...cl.items, newItem] : [newItem],
              }
            : cl
        )
      );
      setNewItemName("");
    } catch (error) {
      console.error("Error creating checklist item:", error);
    }
  };

  const handleAddChecklist = async () => {
    if (!selectedCard || newChecklistName.trim() === "") return;

    try {
      const newChecklist = await createChecklist(
        selectedCard.id,
        newChecklistName.trim()
      );

      setChecklists((prevChecklists) => [...prevChecklists, newChecklist]);
      setNewChecklistName("");
    } catch (error) {
      console.error("Error creating checklist:", error);
    }
  };

  return (
    <div>
      {checklists.map((checklist) => (
        <div key={checklist.id}>
          <ListItemText
            primary={checklist.name}
            style={{ cursor: "pointer" }}
          />
          <ChecklistItem
            selectedCard={selectedCard}
            checklist={checklist}
            handleToggleChecklistItem={handleToggleChecklistItem}
            newItemName={newItemName}
            setNewItemName={setNewItemName}
            handleAddChecklistItem={handleAddChecklistItem}
            handleDeleteChecklistItem={handleDeleteChecklistItem}
          />
        </div>
      ))}

      <TextField
        label="New Checklist"
        value={newChecklistName}
        onChange={(e) => setNewChecklistName(e.target.value)}
        fullWidth
      />
      <Button onClick={handleAddChecklist} variant="contained" color="primary">
        Add Checklist
      </Button>
    </div>
  );
};

Checklist.propTypes = {
  selectedCard: PropTypes.object,
};

export default Checklist;
