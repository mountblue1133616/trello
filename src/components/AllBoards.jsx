import { Link, useNavigate } from "react-router-dom";
import { useState, useEffect } from "react";
import { getAllBoard, createBoard } from "../TrelloAPI";

const AllBoards = () => {
  const [boards, setBoards] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [showForm, setShowForm] = useState(false);
  const [boardName, setBoardName] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    fetchBoards();
  }, []);

  const fetchBoards = () => {
    getAllBoard()
      .then((response) => {
        console.log(response.data);
        setBoards(response.data);
        setIsLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching boards:", error);
        setIsLoading(false);
      });
  };

  const handleCreateBoard = () => {
    createBoard(boardName)
      .then((createdBoard) => {
        setBoards((prevBoards) => [...prevBoards, createdBoard]);
        console.log("Board created successfully:", createdBoard);
        setShowForm(false);
        setBoardName("");
      })
      .catch((error) => {
        console.error("Error creating board:", error);
        console.log(error.response);
      });
  };

  const toggleForm = () => {
    setShowForm(!showForm);
  };

  return (
    <div className="board">
      <h1>Your boards</h1>
      {!showForm ? (
        <button onClick={toggleForm} className="board-add-btn">
          Create Board
        </button>
      ) : (
        <div>
          <input
            type="text"
            value={boardName}
            onChange={(e) => setBoardName(e.target.value)}
            placeholder="Enter board name"
            className="new-board-input"
          />
          <button className="board-add-btn" onClick={handleCreateBoard}>
            Create
          </button>
          <button className="board-add-btn" onClick={toggleForm}>
            Cancel
          </button>
        </div>
      )}
      <div className="board-wrapper">
        {isLoading ? (
          <div>Loading...</div>
        ) : (
          boards.map((board) => (
            <Link
              to={`/boards/${board.id}`}
              key={board.id}
              className="individual-board"
              onClick={() => navigate(`/${board.id}`)}
            >
              {board.name}
            </Link>
          ))
        )}
      </div>
    </div>
  );
};

export default AllBoards;
