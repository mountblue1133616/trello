import { useState } from "react";
import { TextField, Button } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";

const ChecklistItem = ({
  checklist,
  handleToggleChecklistItem,
  newItemName,
  setNewItemName,
  handleAddChecklistItem,
  handleDeleteChecklistItem,
}) => {
  const [isAddingItem, setIsAddingItem] = useState(false);

  console.log("items", checklist.items);

  const handleAddItemClick = () => {
    setIsAddingItem(true);
  };

  const handleCancelAddItemClick = () => {
    setIsAddingItem(false);
    setNewItemName(""); // Reset the new item name
  };

  const handleAddItem = () => {
    handleAddChecklistItem(checklist);
    setIsAddingItem(false);
    setNewItemName(""); // Reset the new item name
  };

  return (
    <div>
      {checklist.items &&
        checklist.items.map((item) => (
          <div key={item.id} style={{ display: "flex", alignItems: "center" }}>
            <label>
              <input
                type="checkbox"
                checked={item.state !== "incomplete"}
                onChange={() => handleToggleChecklistItem(checklist.id, item)}
              />
              {item.name}
            </label>
            <DeleteIcon
              style={{ cursor: "pointer", marginLeft: "auto" }}
              onClick={() => handleDeleteChecklistItem(checklist.id, item.id)}
            />
          </div>
        ))}

      {isAddingItem ? (
        <>
          <TextField
            label="New Item"
            value={newItemName}
            onChange={(e) => setNewItemName(e.target.value)}
            fullWidth
          />
          <Button onClick={handleAddItem} variant="contained" color="primary">
            Add Item
          </Button>
          <Button onClick={handleCancelAddItemClick} color="secondary">
            Cancel
          </Button>
        </>
      ) : (
        <Button
          onClick={handleAddItemClick}
          variant="contained"
          color="primary"
        >
          Add Item
        </Button>
      )}
    </div>
  );
};

export default ChecklistItem;
