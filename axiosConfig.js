import axios from "axios";
import { API_KEY, ACCESS_TOKEN } from "./config";

axios.defaults.baseURL = "https://api.trello.com/1";

axios.defaults.params = {
  key: API_KEY,
  token: ACCESS_TOKEN,
};
export default axios;
